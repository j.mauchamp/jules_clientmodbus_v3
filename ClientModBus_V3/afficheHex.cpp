#include <iostream>
#include <iomanip>      // inclus pour std::setfill et std::setw

using namespace std;

/** ---------------------------------------------------------------
Afficher un tableau de char dans sa repr�sentation hexad�cimale
*texte       : un texte de l�gende
*tableau     : le tableau contenant les char � afficher
longueur     : la longueur du tableau contenant les char � afficher
-------------------------------------------------------------------
**/
void afficheHex(char* texte, char* tableau, int longueur) {
    // La ligne ci-dessous n'affiche rien car il y a des caract�res 0 qui interrompent la chaine
    // cout << "Envoye : " << hex << demande << endl;

    cout << texte;
    for (int i = 0; i < longueur; i++) {
        if (i > 0) {
            cout << ".";
        }
        cout << "0x" << setfill('0') << setw(2) << hex << (short)tableau[i];
    }
    cout << "(en clair : " << tableau << ")";
    cout << endl;

}
