#include "Coil.h"
#include<iostream>
#include <winsock.h>


Coil::Coil(ModBusTcp* Pmodbus, __int8 PnumCoil, bool PonOff)
{
	modBus = Pmodbus;
	numcoil = PnumCoil;
	onOff = PonOff;

	msg[9] = numcoil;

	if (onOff == true)
	{
		Allumer();
	}
}

Coil::~Coil()
{
}

bool Coil::Allumer()
{
	msg[10] = 0xff;
	char outrcx[12];
	modBus->EnvoiParTcp(msg, 12, outrcx, 12);

	onOff = true;
	return true;
}

bool Coil::Eteindre()
{
	msg[10] = 0x00;
	char outrcx[12];
	modBus->EnvoiParTcp(msg, 12, outrcx, 12);

	onOff = false;
	return false;
}
