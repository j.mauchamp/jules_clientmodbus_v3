#pragma once
#include <winsock.h>

class ModBusTcp
{
private:
	char ip[16];
	unsigned int port;
	WSADATA WSAData;
	SOCKET sock;
	SOCKADDR_IN sin;
	int error;

public:

	void AfficherHex(const char* texte, const char* tableau, int longueur);

	int EnvoiParTcp(char* trx, int longueurTrx, char* outRcx, int longueurRcx);

	ModBusTcp(const char* ip, unsigned short port = 502);

	~ModBusTcp();
};

