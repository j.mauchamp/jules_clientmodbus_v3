#pragma once
#include "ModBusTcp.h"
class Coil
{
private:

	__int8 numcoil;
	bool onOff = false;
	ModBusTcp* modBus;
	char msg[12] = { 0x00, 0x06, 0x00, 0x00, 0x00, 0x06, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00 };

public:

	Coil(ModBusTcp* modBus, __int8 numcoil, bool onOff = false);

	~Coil();
	bool Allumer();
	bool Eteindre();
};

