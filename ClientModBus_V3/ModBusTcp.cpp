#include<iostream>
#include <string.h>
#include <iomanip>
#include "ModBusTcp.h"
#include "afficheHex.h"
using namespace std;

void ModBusTcp::AfficherHex(const char* texte, const char* tableau, int longueur) {

    cout << texte;
    for (int i = 0; i < longueur; i++) {
        if (i > 0) {
            cout << ".";
        }
        cout << "0x" << setfill('0') << setw(2) << hex << (short)tableau[i];
    }
    cout << "(en clair : " << tableau << ")";
    cout << endl;

}

ModBusTcp::ModBusTcp(const char* ip, unsigned short port)
{
    this->port = port;
    strncpy_s(this->ip, ip, 16);

    WSAStartup(MAKEWORD(2, 0), &WSAData);

    sock = socket(AF_INET, SOCK_STREAM, 0);

    sin.sin_addr.s_addr = inet_addr(ip);
    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);


    if (connect(sock, (SOCKADDR*)&sin, sizeof(sin)) != 0)
    {
        error = WSAGetLastError();
        std::cout << " Vous n'avez pas pu vous connecter " << WSAGetLastError();
    }
}

ModBusTcp::~ModBusTcp()
{
    closesocket(sock);
    WSACleanup();
}

int ModBusTcp::EnvoiParTcp(char* trx, int longueurTrx, char* outRcx, int longueurRcx)
{
    if (this->sock == INVALID_SOCKET)
    {
        error = WSAGetLastError();
        std::cout << "Erreur initialisation socket : " << WSAGetLastError();
        return-2;
    }

    send(sock, trx, longueurTrx, 0);
    recv(sock, outRcx, longueurRcx, 0);
    AfficherHex("la trame qui a ete envoyee :", trx, longueurTrx);
    AfficherHex("la trame qui a ete recue :", outRcx, longueurRcx);
    cout << endl;
}


