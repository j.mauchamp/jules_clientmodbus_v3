#include <iostream>
#include "ModBusTcp.h"
#include "Coil.h"
#include "lesConstantes.h"
using namespace std;

int main()
{
	ModBusTcp* modbus = new ModBusTcp(ip, port);
	Coil* coil3 = new Coil(modbus, 0x03);
	Coil* coil0 = new Coil(modbus, 0x00);

	int selection;
	do
	{
		cout << " Menu : " << endl;
		cout << "1 - monter" << endl;
		cout << "2 - arret " << endl;
		cout << "3 - descendre " << endl;

		cout << "entrez votre choix : " << endl;
		cin >> selection;

		switch (selection)
		{
		case 1:
			coil0->Allumer();
			coil3->Eteindre();
			break;

		case 2:
			coil3->Eteindre();
			coil0->Eteindre();
			break;

		case 3:
			coil3->Allumer();
			coil0->Eteindre();
			break;

		default:
			return 0;
			break;
		}

	} while (true);

}